hps is a tool for tracking hikes of the peaks on the
[Hundred Peaks Section](http://www.hundredpeaks.org/)'s peak list.

# Feedback

If you have any feedback about the project, please file an issue at
https://gitlab.com/kraai/hps or email kraai@ftbfs.org.

# Installation

It's a Bourne shell script and requires sqlite3, the command-line
interface to the [SQLite 3 database](https://sqlite.org/).

On [Debian](https://www.debian.org/), sqlite3 can be installed by
running

```shell
apt-get install -y sqlite3
```

If you have instructions for other operating systems, please file a
merge request or otherwise let me know.

# Getting started

First, run

```shell
hps init
```

This will create the database, which contains the list of peaks and
HPS routes.

To determine which peaks you haven't hiked yet, run

```shell
hps unhiked
```

When you hike a peak, run

```shell
hps PEAK_ID ROUTE DATE
```

If the hike did not follow an official HPS route, you can omit that
argument.  You should use the ISO-8601 format for the date.  Here's an
example:

```shell
hps add 06N 2 2017-03-19
```

You can run

```shell
hps help
```

for a list of all available commands.
